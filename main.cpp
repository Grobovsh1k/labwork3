#include <iostream>

int Add(int x, int y){
    return x + y;
}

int main() {
    std::cout << "Write arguments\n";
    int x;
    int y;
    std::cin >> x >> y;
    std::cout << Add(x, y) << std::endl;
    return 0;
}
